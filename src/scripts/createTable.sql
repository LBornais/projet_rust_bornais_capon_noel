DROP TABLE IF EXISTS public."cards";

CREATE TABLE public."cards"
(
    id integer NOT NULL,
    name text NOT NULL,
    type character varying(50) NOT NULL,
    "desc" text NOT NULL,
    atk integer,
    def integer,
    level integer,
    race character varying(50) NOT NULL,
    attribute character varying(50)
);