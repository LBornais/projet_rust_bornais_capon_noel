mod models {
    pub mod card;
}

use axum::routing::get;
use models::card::{Card};
use axum::{Json, Router};
use axum::http::StatusCode;

use sqlx::{Pool, Postgres, postgres::PgPoolOptions};
use dotenvy::dotenv;
use tokio::net::TcpListener;
use std::env;
use tokio;

const BASE_URL: &str = " https://db.ygoprodeck.com/api/v7/randomcard.php";

#[tokio::main]
async fn main() {
    dotenv().ok();
    let app = Router::new().route("/", get(random_card_handler));

    let listener = TcpListener::bind("127.0.0.1:3000").await.unwrap();
    axum::serve(listener, app.into_make_service())
        .await
        .unwrap();

}

async fn get_random_card() -> (StatusCode, Json<Card>) {
    match reqwest::get(BASE_URL).await {
        Ok(response) => {
            match response.json::<Card>().await {
                Ok(card) => (StatusCode::OK, Json(card)),
                Err(err) => {
                    eprintln!("Failed to parse JSON: {:?}", err);
                    (StatusCode::INTERNAL_SERVER_ERROR, Json(Card::default()))
                }
            }
        }
        Err(err) => {
            eprintln!("Failed to make request: {:?}", err);
            (StatusCode::INTERNAL_SERVER_ERROR, Json(Card::default()))
        }
    }
}

async fn random_card_handler() -> Json<Card> {
    let card = get_random_card().await;
    match save_card(&get_pool().await, &card.1).await {
        Ok(_) => card.1,
        Err(err) => {
            eprintln!("Failed to save card: {:?}", err);
            Json(Card::default())
        }
    }
}

async fn get_pool() -> Pool<Postgres> {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await
        .expect("Failed to create pool")
}

fn truncate_string(s: &str, max_len: usize) -> String {
    if s.len() > max_len {
        s[..max_len].to_string()
    } else {
        s.to_string()
    }
}

async fn save_card(pool: &Pool<Postgres>, card: &Card) -> Result<(), sqlx::Error> {
    let truncated_name = truncate_string(&card.name, 50);
    let truncated_desc = truncate_string(&card.desc, 255);

    sqlx::query(
        r#"
        INSERT INTO cards (id, name, type, "desc", atk, def, level, race, attribute)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
        "#,
    )
    .bind(card.id)
    .bind(&truncated_name)
    .bind(&card.r#type)
    .bind(&truncated_desc)
    .bind(card.atk)
    .bind(card.def)
    .bind(card.level)
    .bind(&card.race)
    .bind(&card.attribute)
    .execute(pool)
    .await?;
    Ok(())
}

async fn get_card_by_id(pool: &Pool<Postgres>, id: i32) -> Result<Card, sqlx::Error> {
    let card = sqlx::query_as!(
        Card,
        r#"
        SELECT id, name, type, "desc", atk, def, level, race, attribute
        FROM cards
        WHERE id = $1
        "#,
        id
    )
    .fetch_one(pool)
    .await?;
    
    Ok(card)
}

#[tokio::test]
async fn card_id_incrementing_test() {
    let card = get_random_card().await.1.0;

    assert!(card.id > 0);
}

#[tokio::test]
async fn save_and_retrieve_card_test() {
    let pool = get_pool().await;

    let (status, card) = get_random_card().await;
    assert_eq!(status, StatusCode::OK, "Le statut de la réponse doit être OK");

    save_card(&pool, &card.0).await.expect("La sauvegarde de la carte doit réussir");

    let saved_card = get_card_by_id(&pool, card.0.id)
        .await
        .expect("La récupération de la carte doit réussir");

    assert_eq!(card.0.id, saved_card.id, "Les cartes doivent être identiques");
}
