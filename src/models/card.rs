use serde::Serialize;
use serde::Deserialize;

#[derive(Debug, Serialize, Deserialize)]
pub struct Card{
    pub id: i32,
    pub name: String,
    pub r#type: String,
    pub desc: String,
    pub atk: Option<i32>,
    pub def: Option<i32>,
    pub level: Option<i32>,
    pub race: String,
    pub attribute: Option<String>
}

impl Default for Card {
    fn default() -> Self {
        Card {
            id: 0,
            name: "Default Card".to_string(),
            r#type: "Default".to_string(),
            desc: "This is a default card".to_string(),
            atk: None,
            def: None,
            level: None,
            race: "Default".to_string(),
            attribute: None
        }
    }
}