# Les Spider-Fraudes
- Bornais Lorenzo
- Capon Tom
- Noël Valentin

# Descriptif

Récupération des cartes Yu-Gi-Oh via une API existante et enregistrement dans la BDD

# Création de la BDD

- Installer PostreSQL/pgAdmin
- Créer la BDD yugioh
- Jouer le script createTable.sql